<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Validator;

class LinkController extends Controller
{
    /**
     * Display the default page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('shorten');
    }

    /**
     * Store a shortened URL.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	// Get all input from the present session on the url shortener form and page
        $input = $request->all();

        // Simple implementation of validation for the url shortener form. This validation must pass before the code can be executed.
        $validator = Validator::make($input, [
            'url' => 'required|url|min:20',
        ]);

        if ($validator->fails()) {
        	// If validation fails, it redirects back to the previous page and displays the errors.
            return redirect()->back()->withErrors($validator);

        } else {
            // When validation passes, it executes the codes to shorten the url
            $url = Link::whereUrl($request->get('url'))->first();

            if ($url) {
                // This check is the url entered already exists in our database, if it does it redirects back to the previous page with the shortened url in the database.
                return redirect()->back()->withInput()->with('message', 'Shortened Link Already exists: <a href="'.url("/{$url->code}").'" target="_blank">"'.url("/{$url->code}").'"</a>');

            } else {
                do {
                    // Generate hash codes for the URL
                    $newHash = Str::random(6);

                } while (Link::where('code', '=', $newHash)->count() > 0);

                // We use the eloquent builder to store our values into our database.
                $link = new Link([
                    'url' => $request->get('url'),
                    'code' => $newHash,
                ]);

                $link->save();

                // After shortened url has been created, we display the url to the user.
                return redirect()->back()->withInput()->with('message', 'Here is your shortened url: <a href="'.url("/{$link->code}").'" target="_blank">"'.url("/{$link->code}").'"</a>');
            }

        }
    }


    public function get($code)
    {
    	// We check if the hash supplied to the url exists in the database
        $link = Link::where('code','=',$code)->first();

        if ($link) {
            // If the hash exists, we get the url associated with the code and redirect the user to the shortened url's original url.
        	return redirect($link->url);

        } else {
            // If the hash given is invalid we redirect the user to the home page with the error that the link supplied is invalid.
        	return redirect('/')->withErrors('Invalid Link');
        }
    }
}
