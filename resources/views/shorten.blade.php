<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Laravel</title>
    <style>
      @import url("https://fonts.googleapis.com/css?family=Montserrat");

      html {
        box-sizing: border-box;
      }

      *,
      *:before,
      *:after {
        box-sizing: inherit;
        padding: 0;
        margin: 0;
      }

      body {
        font-family: "Montserrat", sans-serif;
        width: 100vw;
        height: 100vh;
        padding: 0;
        margin: 0;
        display: flex;
        overflow: hidden;
        opacity: 1;
      }

      body::before {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-image: linear-gradient(to left, #ef87b5 1%, #75bdea 100%);
        opacity: 0.1;
      }

      main::before {
        content: "";
        width: calc(100% - 3rem);
        height: calc(100% - 3rem);
        position: absolute;
        background-color: #ef87b5;
        clip-path: polygon(100% 0, 0% 100%, 100% 100%);
        opacity: 0.2;
      }

      main {
        width: calc(100% - 3rem);
        height: calc(100% - 3rem);
        overflow-y: auto;
        z-index: 1;
        margin: auto;
        background-color: #ececec;
        border-radius: 1.5rem;
        display: grid;
        grid-template-columns: auto;
        grid-template-rows: auto;
      }

      section {
        display: grid;
        grid-template-columns: 1fr auto;
        grid-template-rows: auto;
        height: auto;
        align-self: center;
        margin: 0 3rem;
      }

      form {
        display: grid;
        grid-template-columns: 0.2fr auto;
      }

      input,
      button {
        perspective: 100rem;
        border-radius: 3rem;
        transform: rotateY(-30deg) rotateX(15deg);
        border-bottom: 0.3rem solid rgba(0, 0, 0, 0.3);
        height: 3rem;
        color: #ff006d;
        font-weight: bold;
        mix-blend-mode: multiply;
        transition: all 1s ease-in-out;
      }

      input:hover,
      button:hover {
        transform: rotateY(0) rotateX(0);
      }

      button {
        cursor: pointer;
      }

      .success {
          color: green;
      }

      .error {
          color: red;
      }
    </style>
  </head>

  <body>
    <main>
      <section>
        <div>
          <h1>Tiny&Cute</h1>
          <p>Every URL that's tiny is cute.</p>
        </div>
        <form action="/store" method="post">
          @csrf
          <input name="url" type="url" placeholder="URL" value="{{ old('url') }}" required />
          <button type="submit">Make me cute!</button>
        </form>
        {{-- Code to show all errors that occured during the shortening process. --}}
        @foreach($errors->all() as $error)
        <p class="error">{{ $error }}</p>
        @endforeach
        {{-- Code to get any message that the shortening proccess outputs, both shortened urls and runtime messages. --}}
        @if(session('message'))
        <p class="success">{!! session('message') !!}</p>
        @endif
      </section>
    </main>
  </body>
</html>
