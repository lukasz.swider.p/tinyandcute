<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Link::class, function (Faker $faker) {
    return [
        'url' => $faker->url,
        'code' => Str::random(6),
    ];
});
